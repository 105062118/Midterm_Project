window.onload = function(){
    personal_data_init();
}

function personal_data_init(){
    var loginuser;
    var photourl,name,introduction,uid,email;
    var first_count = 0;
    var second_count = 0;
    var database;
    //大頭貼
    document.getElementById("photoInput").addEventListener("change",function(){
        readURL(this);
    });
    //取得currentuser
    firebase.auth().onAuthStateChanged(function(user){
        if(user){
            loginuser = user;
            uid = user.uid;
        }
        else{
            loginuser = null;
        }
    });
    //取得user資料
    var database = firebase.database().ref('users');
    database.once('value', function(snapshot){
        snapshot.forEach(function(childSnapshot){
            var temp_data = childSnapshot.val();
            if(temp_data.uid == uid){           
                photourl = temp_data.photourl;
                console.log("獲得大頭貼url");
            }
        });
        document.getElementById("userphoto").src =photourl;
        document.getElementById("changephoto").src = photourl;

        //通知
        if (Notification.permission === 'default' || Notification.permission === 'undefined') {
            Notification.requestPermission(function (permission) {
            });
        }
        firebase.database().ref('room/'+uid).once('value', function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var temp_data = childSnapshot.val();
                first_count+=1;
            });

            firebase.database().ref('room/'+uid).on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    console.log("新訊息")
                    var temp_data = data.val();
                    var notification_name = temp_data.name;
                    var notifyConfig = {
                        body : temp_data.data,
                        icon : temp_data.photourl
                    };
                    if (Notification.permission === 'granted') { // 使用者同意授權
                        var notify = new Notification(notification_name+"在你聊天室留言", notifyConfig);
                        console.log('成功跳出通知');
                        notify.onclick = function(){
                            console.log('點擊通知');
                            firebase.database().ref('roomnumber').set({
                                uid : uid
                            }).then(function(){
                                document.location = 'mychatroom.html';
                            });
                        }
                    }
                }
            });
        });
    });
    //儲存資料
    document.getElementById("save").addEventListener("click",function(){
        name = document.getElementById("name").value;
        if(name != ""){
            database = firebase.database().ref('users/'+loginuser.uid);
            photourl =document.getElementById("changephoto").src;
            uid = loginuser.uid;
            email = loginuser.email;
            document.getElementById("userphoto").src =photourl;
            document.getElementById("changephoto").src = photourl;
            var text_str = name;
            var text_1 = text_str.replace('<','&lt');
            console.log("寫入個人資料");
            database.set({
                name:text_1,
                email:email,
                uid : uid,
                photourl:photourl
            });
            alert("更改成功");
            document.location = 'index.html';
        }
    });
}

function readURL(input){
    if(input.files && input.files[0]){
      var reader = new FileReader();
      reader.onload = function (e) {
         $("#changephoto").attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }