window.onload = function(){
    friend_list_init();
}

var uid_templist = [];
var button_tempcount;

function friend_list_init(){
    var loginuser;
    var first_count = 0;
    var second_count = -1;
    var button_count = 0;
    var per_first_count = 0;
    var per_second_count = 0;
    var photourl,name,email;
    var uid;
    var uid_list = [];
    var str_1 = "<div style = 'margin: 0px auto;width:600px;height:180px;border:3px; background-color: darkgrey;margin-top:10px;'><div style = 'display: inline-block;float:left'><img class = 'photo' src ='";
    var str_2 = "' style = 'width:175px;height:175px;'></div><div style = 'display: inline-block;float:left;'><h2>Name :<strong> ";
    var str_3 = "</strong></h2><h2>Email : <strong>";
    var str_4 = "</strong></h2></div><div style = 'display: inline-block;float:right;'><button type ='button' class='' id ='button";
    var str_5 = "' style = 'float:right;' onclick = chatbutton(";
    var str_6 = ")>跟他聊天</button></div></div>";
    var friend_list = [];

    var datanode = firebase.database().ref('users');
    //取得currentuser
    firebase.auth().onAuthStateChanged(function(user){
        if(user){
            loginuser = user;
            uid = user.uid;
        }
        else{
            loginuser = null;
        }
    });

    datanode.once('value', function(snapshot){
        snapshot.forEach(function(childSnapshot){
            var temp_data = childSnapshot.val();
            photourl = temp_data.photourl;
            if(temp_data.uid == uid){           
                document.getElementById("userphoto").src =photourl;
            }
            else{
                console.log("讀取用戶資料");
                var button_number = button_count.toString();
                var str = str_1 + photourl + str_2 + temp_data.name + str_3 + temp_data.email + str_4 + button_number +str_5 +button_number +str_6;
                friend_list[friend_list.length] = str;
                uid_list[button_count] = temp_data.uid;
                first_count+=1;
                button_count+=1;
                console.log(first_count);
            }
        });

        document.getElementById('friend').innerHTML = friend_list.join('');

        datanode.on('child_added', function (data) {
            button_number = button_count.toString();
            second_count += 1;
            photourl = data.photourl;
            if (second_count > first_count) {
                console.log("新增用戶資料2")
                var temp_data = data.val();
                friend_list[friend_list.length] = str_1 + photourl + str_2 + temp_data.name + str_3 + temp_data.email + str_4 + button_number +str_5;
                document.getElementById('friend').innerHTML = friend_list.join('');
                uid_list[button_count] = temp_data.uid;
                button_count+=1;
            }
        });

        //通知
        if (Notification.permission === 'default' || Notification.permission === 'undefined') {
            Notification.requestPermission(function (permission) {
            });
        }
        console.log(uid);
        firebase.database().ref('room/'+uid).once('value', function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var temp_data = childSnapshot.val();
                per_first_count+=1;
            });

            firebase.database().ref('room/'+uid).on('child_added', function (data) {
                per_second_count += 1;
                if (per_second_count > per_first_count) {
                    console.log("新訊息")
                    var temp_data = data.val();
                    var notification_name = temp_data.name;
                    var notifyConfig = {
                        body : temp_data.data,
                        icon : temp_data.photourl
                    };
                    if (Notification.permission === 'granted') { // 使用者同意授權
                        var notify = new Notification(notification_name+"在你聊天室留言", notifyConfig);
                        console.log('成功跳出通知');
                        notify.onclick = function(){
                            console.log('點擊通知');
                            firebase.database().ref('roomnumber').set({
                                uid : uid
                            }).then(function(){
                                document.location = 'mychatroom.html';
                            });
                        }
                    }
                }
            });
        });
    });
    uid_templist = uid_list;
    button_tempcount = button_count;
}

function chatbutton(button_count){
    var uid = uid_templist[button_count];
    firebase.database().ref('roomnumber').set({
        uid : uid
    });
    document.location = 'mychatroom.html';
}