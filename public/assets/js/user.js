window.onload = function(){
    user_init();
}

function user_init(){
    var loginuser;
    var account = document.getElementById("account");
    var chatroom = document.getElementById("chatroom");
    var friend_list = document.getElementById("friend_list");
    var popcorn_chatroom = document.getElementById("popcorn_chatroom");
    var introduction = document.getElementById("introduction");
    var personal_information = document.getElementById("personal_information");
    var develop_record = document.getElementById("develop_record");
    var name,email,photourl,uid,emailverified,uid;
    var user_exist = false;
    var user_permission;
    var first_count = 0;
    var second_count = 0;
    firebase.auth().onAuthStateChanged(function(user){
        if(user){
            loginuser = user;
            //user_information
            name = user.displayName;
            email = user.email;
            uid = user.uid;
            photourl ="images/avatar.jpg";
            //檢驗是否為第一次登入
            var database = firebase.database().ref('users');

            database.once('value', function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    var temp_data = childSnapshot.val();
                    if(temp_data.uid == uid){           
                        user_exist = true;
                        photourl = temp_data.photourl;
                        console.log("user has existed");
                    }
                });
                //第一次登入
                if(user_exist == false){
                    console.log("write user's data")
                    firebase.database().ref('users/'+user.uid).set({
                        name :"New user",
                        email:email,
                        uid : uid,
                        photourl :"images/avatar.jpg"
                    });
                    console.log("初始化大頭貼");
                    document.getElementById("userphoto").src ="images/avatar.jpg";
                }
                else{
                    console.log("初始化已設定大頭貼");
                    document.getElementById("userphoto").src =photourl;
                }
            });
            console.log("user is logined");
            //各功能連結
            document.getElementById("account").href = "signin.html";
            document.getElementById("sign_in_out").innerHTML = "登出/切換帳號"
            document.getElementById("chatroom").href ="mychatroom.html";
            document.getElementById("friend_list").href = "friend_list.html";
            document.getElementById("personal_data").href = "personal_data.html";
            document.getElementById("popcorn_chatroom").href = "mychatroom.html"
        }
        else{
            loginuser = null;
            console.log("user is not logined yet");
            document.getElementById("account").href = "signin.html";
            document.getElementById("sign_in_out").innerHTML = "登入";
            document.getElementById("chatroom").href ="index.html";
            document.getElementById("friend_list").href = "index.html";
            document.getElementById("personal_data").href = "index.html";
        }

        //訊息通知
        if (Notification.permission === 'default' || Notification.permission === 'undefined') {
            Notification.requestPermission(function (permission) {
            });
        }
        firebase.database().ref('room/'+uid).once('value', function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var temp_data = childSnapshot.val();
                first_count+=1;
            });

            firebase.database().ref('room/'+uid).on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    console.log("新訊息")
                    var temp_data = data.val();
                    var notification_name = temp_data.name;
                    var notifyConfig = {
                        body : temp_data.data,
                        icon : temp_data.photourl
                    };
                    if (Notification.permission === 'granted') { // 使用者同意授權
                        var notify = new Notification(notification_name+"在你聊天室留言", notifyConfig);
                        console.log('成功跳出通知');
                        notify.onclick = function(){
                            console.log('點擊通知');
                            firebase.database().ref('roomnumber').set({
                                uid : uid
                            }).then(function(){
                                document.location = 'mychatroom.html';
                            });
                        }
                    }
                }
            });
        });
    });

    account.addEventListener('click',function(){
        console.log("logout first");
        if(loginuser){
            firebase.auth().signOut().then(function(){
                console.log("logout");
                alert("Logout success");
            }).catch(function(){
                console.log("logout fail")
                alert("Logout fail");
            });
        }
    });

    popcorn_chatroom.addEventListener('click',function(){
        firebase.database().ref('roomnumber').set({
            uid : "vfGiy73utpYhPIRyCJWhk65l91z1"
        });
    });
    
    chatroom.addEventListener('click',function(){
        firebase.database().ref('roomnumber').set({
            uid : uid
        });
    });
}


