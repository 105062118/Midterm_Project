window.onload = function(){
    init();
}

function init(){
    var email = document.getElementById("inputEmail");
    var password = document.getElementById("inputPassword");

    //sign in with email
    document.getElementById("btnLogin").addEventListener("click",function(){
        firebase.auth().signInWithEmailAndPassword(email.value,password.value).then(function(e){
            console.log("sign in success");
            alert("Login success");
            document.location.href ="index.html"
        }).catch(function(e){
            console.log("sign in error");
            email.value = "";
            password.value = "";
        });
    });

    //sign in with google
    var provider = new firebase.auth.GoogleAuthProvider();
    var token;
    var user;
    document.getElementById("btngoogle").addEventListener("click",function(){
        firebase.auth().signInWithPopup(provider).then(function(result){
            var token = result.credential.accessToken;
            var user = result.user;
            console.log("sign in with google");
            document.location.href ="index.html"
        }).catch(function(error){
            console.log("sign in error :"+error.message);
        });
    });

    //sign up
    document.getElementById("btnSignUp").addEventListener("click",function(){
        firebase.auth().createUserWithEmailAndPassword(email.value,password.value).then(function(e){
            console.log("sign up success");
            //新增登入者資料          
            /*firebase.database().ref('users/' + loginuser.uid).set({
                email: loginuser.email,
              }).catch(function(error){
                console.error("寫入使用者資訊錯誤",error);
            });*/
            document.location.href ="index.html"
        }).catch(function(e){
            console.log("sign up error" + e.message);
        });
    });
}