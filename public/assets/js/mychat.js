window.onload = function(){
    chat_init();
}

function chat_init(){
    var btnsubmit = document.getElementById("submit");
    var input = document.getElementById("content");
    var database = firebase.database().ref('users');
    var roomnumber = firebase.database().ref('roomnumber');
    var loginuser,uid;
    var photourl,name;
    var owner_uid;
    var content_list = [];
    var first_count = 0;
    var second_count = 0;
    var per_first_count = 0;
    var per_second_count = 0;
    var str;
    var str_1_right = "<div class='mwt_border_right'><span class='arrow_r_int'></span><span class='arrow_r_out'></span><div class = 'phh'>";
    var str_1_left = "<div class='mwt_border_left'><span class='arrow_l_int'></span><span class='arrow_l_out'></span><div class = 'phh'>";
    var str_2 = "<img id = 'ph' src = '";
    var str_3 = "'></div><div class = 'content_name'><h4 ><strong>";
    var str_4 = "</strong></h4></div><div class = 'content_details'><h4>"
    var str_5 = "</h4></div></div>";
    var roomnode;
    var database = firebase.database().ref('users');
    
    //取得currentuser
    firebase.auth().onAuthStateChanged(function(user){
        if(user){
            loginuser = user;
            uid = user.uid;
        }
        else{
            loginuser = null;
        }
        //取得房間擁有者
        roomnumber.once('value').then(function(snapshot){
            owner_uid = snapshot.val().uid;
            //取得使用者大頭貼和名字
            database.once('value', function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    var temp_data = childSnapshot.val();
                    if(temp_data.uid == uid){
                        photourl = temp_data.photourl;
                        name = temp_data.name;
                    }
                    if(temp_data.uid == owner_uid){
                        document.getElementById("announce").innerHTML = "Here is "+temp_data.name+"'s Chatroom";
                    }
                });
                document.getElementById("userphoto").src =photourl;

                //讀取聊天內容
                firebase.database().ref('room/'+owner_uid).once('value').then(function(snapshot){
                    snapshot.forEach(function(childSnapshot){
                        var temp_data = childSnapshot.val();
                        if(loginuser.uid == temp_data.user_uid){
                            str = str_1_right + str_2 + temp_data.photourl + str_3 + temp_data.name +'  '+temp_data.time + str_4 +temp_data.data+str_5;
                        }
                        else{
                            str = str_1_left + str_2 + temp_data.photourl + str_3 + temp_data.name +'  '+temp_data.time + str_4 +temp_data.data+str_5;
                        }
                        console.log("讀取聊天紀錄");
                        content_list[content_list.length] = str;
                        first_count+=1;
                    });
                    console.log(first_count);
                    document.getElementById('chat').innerHTML = content_list.join('');
            
                    firebase.database().ref('room/'+owner_uid).on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var temp_data = data.val();
                            if(uid == temp_data.user_uid){
                                console.log("自己說話")
                                str = str_1_right + str_2 + temp_data.photourl + str_3 + temp_data.name +'  '+temp_data.time + str_4 +temp_data.data+str_5;
                            }
                            else{
                                console.log("別人說話");
                                str = str_1_left + str_2 + temp_data.photourl + str_3 + temp_data.name +'  '+temp_data.time + str_4 +temp_data.data+str_5;
                            }
                            content_list[content_list.length] = str;
                            document.getElementById('chat').innerHTML = content_list.join('');
                        }
                    });
                    if (Notification.permission === 'default' || Notification.permission === 'undefined') {
                        Notification.requestPermission(function (permission) {
                        });
                    }
                    firebase.database().ref('room/'+uid).once('value', function(snapshot){
                        snapshot.forEach(function(childSnapshot){
                            var temp_data = childSnapshot.val();
                            per_first_count+=1;
                        });
            
                        firebase.database().ref('room/'+uid).on('child_added', function (data) {
                            per_second_count += 1;
                            if (per_second_count > per_first_count) {
                                console.log("新訊息")
                                var temp_data = data.val();
                                var notification_name = temp_data.name;
                                var notifyConfig = {
                                    body : temp_data.data,
                                    icon : temp_data.photourl
                                };
                                if (Notification.permission === 'granted') { // 使用者同意授權
                                    var notify = new Notification(notification_name+"在你聊天室留言", notifyConfig);
                                    console.log('成功跳出通知');
                                    notify.onclick = function(){
                                        console.log('點擊通知');
                                        firebase.database().ref('roomnumber').set({
                                            uid : uid
                                        }).then(function(){
                                            document.location = 'mychatroom.html';
                                        });
                                    }
                                }
                            }
                        });
                    });
                });
            });
        });
    });
    var t = $('.content_details').text();
    $('.content_details').text(t);

    //input
    btnsubmit.addEventListener('click',function(){
            if(input.value != ""){
            var date =new Date();
            var h = date.getHours();
            var m = date.getMinutes();
            var now;
            var text_str = input.value;
            var text_1 = text_str.replace('<','&lt');
            if(h>12){
                h = h-12;
                if(m < 10)
                    now = "下午"+" "+h+" : "+"0"+m;
                else
                    now = "下午"+" "+h+" : "+m;
            }
            else{
                if(h==0){
                    now = '上午'+' '+'0'+h+' '+m;
                }
                else{
                    now = "上午"+" "+h+" : "+m;
                }
            }

            var data = {
                name : name,
                photourl : photourl,
                time : now,
                data : text_1,
                user_uid : loginuser.uid
            }
            firebase.database().ref('room/'+owner_uid).push(data).catch(function(){
                console.log("點擊傳送失誤");
            });
            input.value = "";
        }
    });
    input.addEventListener('keydown',function(e){
        if(e.keyCode == 13){
            if(input.value != ""){
                var date =new Date();
                var h = date.getHours();
                var m = date.getMinutes();
                var now;
                var text_str = input.value;
                var text_1 = text_str.replace('<','&lt');
                if(h>12){
                    h = h-12;
                    if(m < 10)
                    now = "下午"+" "+h+" : "+"0"+m;
                    else
                    now = "下午"+" "+h+" : "+m;
                }
                else{
                    if(h==0){
                        now = '上午'+' '+'0'+h+' '+m;
                    }
                    else{
                        now = "上午"+" "+h+" : "+m;
                    }
                }

                var data = {
                    name : name,
                    photourl : photourl,
                    time : now,
                    data : text_1,
                    user_uid : loginuser.uid
                }   
                firebase.database().ref('room/'+owner_uid).push(data).catch(function(){
                    console.log("Enter傳送失誤");
                });
                input.value = "";
            }
        }
    });
}

